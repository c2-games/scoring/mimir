FROM debian as builder

RUN apt update && apt upgrade -y
RUN apt install -y libczmq-dev libguestfs-dev gcc valgrind gdb make pkg-config

COPY . /opt
WORKDIR /opt

FROM builder as compile

RUN make
