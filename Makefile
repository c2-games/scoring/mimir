build: daemon client
debug: daemon-debug client-debug

daemon:
	gcc -Wall cmd/mimird.c ./src/* -o mimird `pkg-config libguestfs libczmq --cflags --libs` -Iinclude

client:
	gcc -Wall client-examples/client-mimir.c ./src/* -o c-mimir `pkg-config libguestfs libczmq --cflags --libs` -Iinclude

daemon-debug:
	gcc -ggdb3 -Wall cmd/mimird.c ./src/* -o mimird `pkg-config libguestfs libczmq --cflags --libs` -Iinclude

client-debug:
	gcc -ggdb3 -Wall client-examples/client-mimir.c ./src/* -o c-mimir `pkg-config libguestfs libczmq --cflags --libs` -Iinclude

clean:
	rm -f c-mimir mimird
