#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <czmq.h>
#include <zmsg.h>
#include <zactor.h>
#include <guestfs.h>
#include <time.h>

#include "libguestfs-inspect.h"
#include "guestfs-daemonlibc.h"
#include "mimir-guestfs.h"

typedef struct {
  char *name;
  zactor_t *worker;
} zactor_worker_map;

typedef struct {
  zactor_worker_map ***wmap;
  size_t *wmap_size;
} management_worker_ref;

static void worker_task(zsock_t*, const char*);
static void management_worker_task(zsock_t *pipe, management_worker_ref *map_ref);
static void log_message(const char *message);
static void mapping_zactor_worker(const char *name, zactor_t *worker, zactor_worker_map **map, size_t mapper_size);
static void print_zactor_worker(zactor_worker_map **map, size_t mapper_size);
zactor_t * zactor_worker_lookup(char *name, zactor_worker_map**, size_t);

static void print_mimir_intro() {
  printf("\n"
"\n"
"ooo        ooooo  o8o                     o8o           \n"
"`88.       .888\'  `\"\'                     `\"\'           \n"
" 888b     d\'888  oooo  ooo. .oo.  .oo.   oooo  oooo d8b \n"
" 8 Y88. .P  888  `888  `888P\"Y88bP\"Y88b  `888  `888\"\"8P \n"
" 8  `888\'   888   888   888   888   888   888   888     \n"
" 8    Y     888   888   888   888   888   888   888     \n"
"o8o        o888o o888o o888o o888o o888o o888o d888b \n"

"\n");
}

static void worker_task(zsock_t *pipe, const char *disk_path) {
  guestfs_h *g;
  g = init_ro_guestfs(disk_path);

  zsock_t *worker = zsock_new_dealer("inproc://workers");

  // ZeroMQ Opens here
  zsock_signal(pipe, 0);

  while (true) {
    zmsg_t *msg = zmsg_recv(pipe);
    if (!msg) {
      break;
    }

    zframe_t *client_id = zmsg_pop(msg);
    // Checking for term message
    if (zframe_streq(client_id, "$TERM")) {
      zframe_destroy(&client_id);
      zmsg_destroy(&msg);
      break;
    }

    struct guestfs_inpsect_command *command = guestfs_inspect_zmsg_to_command(msg);

    zmsg_t *reply = zmsg_new();
    if (!reply) {
      printf("wuddahec\n");
      exit(EXIT_FAILURE);
    }

    if (command == NULL) {
      zmsg_pushstr(reply, "Command unable to be parsed");
    } else { // Process command
      switch (command->command) {
        case GUESTFS_COMMAND_LS:;
          zmsg_pushstr(reply, "From worker! -- not implemented");
          break;
        case GUESTFS_COMMAND_CAT:;
          char *res = NULL;
          size_t s = 0;
          cat_file(g, command->args.cat.paths[0], &res, &s);
          zmsg_addmem(reply, res, s);
          free(res);
          break;
        case GUESTFS_COMMAND_STATUS:;
          int status = guestfs_is_ready(g);
          if (status) {
            char *guest_type = mimir_guestfs_os_type(g);
            if (guest_type != NULL) {
              zmsg_pushstr(reply, guest_type);
            } else {
              zmsg_pushstr(reply, "NULL");
            }
          } else {
            zmsg_pushstr(reply, "NULL");
          }
          break;
        default:;
          zmsg_pushstr(reply, "Unknown command");
          break;
      }
    }

    // Sending reply
    zmsg_pushstr(reply, "");
    zmsg_prepend(reply, &client_id);
    zmsg_send(&reply, worker);

    if (command != NULL) { // Destroy if command was successfully parsed
      guestfs_inspect_command_destroy(&command);
    }

    zframe_destroy(&client_id);
    zmsg_destroy(&msg);
    zmsg_destroy(&reply);
  }

  guestfs_shutdown(g);
  guestfs_close(g);
  zsock_destroy(&worker);
  exit(EXIT_SUCCESS);
}

static void management_worker_task(zsock_t *pipe, management_worker_ref *map_ref) {
  zsock_t *management_socket = zsock_new(ZMQ_ROUTER);
  zsock_bind(management_socket, "ipc:///tmp/guestfs-mang-inspect.sock");

  zsock_signal(pipe, 0);

  zactor_worker_map **map = *map_ref->wmap;
  size_t *map_size = &*map_ref->wmap_size;

  zpoller_t *poller = zpoller_new(pipe, management_socket, NULL);
  assert(poller);

  zframe_t *message_pop = NULL;

  while (true) {
    void *sock = zpoller_wait(poller, -1);

    if (!sock) {
      break;
    }

    if (sock == pipe) {
      zmsg_t *msg = zmsg_recv(pipe);

      zmsg_destroy(&msg);
    } else if (sock == management_socket) {
      zmsg_t *msg = zmsg_recv(management_socket);

      zframe_t *identity = zmsg_pop(msg);

      // Popping NULL frame
      message_pop = zmsg_pop(msg);
      zframe_destroy(&message_pop);

      // Determine message
      char *message_type_str = NULL;
      uint16_t request_type = 0;
      zframe_t *zmessage_type = zmsg_pop(msg);
      message_type_str = zframe_strdup(zmessage_type);

      char *tmp_end = NULL;
      long message_type = strtol(message_type_str, &tmp_end, 10);
      if (tmp_end == message_type_str ||
          *tmp_end != '\0' ||
          message_type < 0 ||
          message_type >= 0x10000) {
        printf("message too long \n");
      } else {
        request_type = (uint16_t)message_type;
      }

      zframe_destroy(&zmessage_type);

      zmsg_t *resp = zmsg_new();

      switch(request_type) {
        case 0:; // Unknown operation
          zmsg_pushstr(resp, "Unknown command type");
          break;
        case 1:; // Create new worker
          zframe_t *client_name = zmsg_pop(msg);
          char *client = zframe_strdup(client_name);

          zframe_t *path_name = zmsg_pop(msg);
          char *path = zframe_strdup(path_name);

          // Fail out if the worker already exists
          if (zactor_worker_lookup(client, (*map_ref->wmap), *map_size) != NULL) {
            zmsg_pushstr(resp, "Worker with that name already exists");
          } else {
            zmsg_pushstr(resp, "Create worker");

            *map_size = *map_size + 1;
            map = realloc(map, sizeof(zactor_worker_map *) * *map_size);
            // Setting new map addr
            map_ref->wmap = &map;
            // Setting new map addr
            mapping_zactor_worker(client, zactor_new((void *)worker_task, path), map, *map_size);
          }

          free(client);
          free(path);
          zframe_destroy(&client_name);
          zframe_destroy(&path_name);
          break;
        case 2:; // Tag existing worker
          zframe_t *worker_name = zmsg_pop(msg);
          char *worker_name_char = zframe_strdup(worker_name);

          zframe_t *worker_tag = zmsg_pop(msg);
          char *worker_tag_char = zframe_strdup(worker_tag);

          zactor_t *lookup_worker = zactor_worker_lookup(worker_name_char, (*map_ref->wmap), *map_size);
          zactor_t *lookup_tag = zactor_worker_lookup(worker_tag_char, (*map_ref->wmap), *map_size);

          if (lookup_worker == NULL) {
            zmsg_pushstr(resp, "Worker does not exist");
          } else if (lookup_tag != NULL) {
            zmsg_pushstr(resp, "Tag already exists");
          } else {
            zmsg_pushstr(resp, "Tagging worker");
            *map_size = *map_size + 1;
            map = realloc(map, sizeof(zactor_worker_map *) * *map_size);
            // Setting new map addr
            map_ref->wmap = &map;
            // Setting new map addr
            mapping_zactor_worker(worker_tag_char, lookup_worker, map, *map_size);
          }

          free(worker_tag_char);
          free(worker_name_char);
          zframe_destroy(&worker_name);
          zframe_destroy(&worker_tag);
          break;
        case 32:; // Worker list
          // TODO: Start tracking worker loaded paths
          print_zactor_worker(map, *map_size);
          break;
      }

      // Response
      // Sending response
      zmsg_pushstr(resp, "");
      zmsg_prepend(resp, &identity);
      zmsg_send(&resp, management_socket);

      free(message_type_str);
      zmsg_destroy(&msg);
    } else {
      log_message("Unknown message in management_sock");
    }

  }
}

char *strdup(const char *);
static void mapping_zactor_worker(const char *name, zactor_t *worker, zactor_worker_map **map, size_t mapper_size) {
  map[mapper_size - 1] = malloc(sizeof(zactor_worker_map));
  (*map[mapper_size - 1]).name = strdup(name);
  (*map[mapper_size - 1]).worker = worker;
  return;
}

static void print_zactor_worker(zactor_worker_map **map, size_t map_size) {
  log_message("Printing all workers");
  for (size_t i = 0; i < map_size; i++) {
    log_message((*map[i]).name);
  }
}

zactor_t * zactor_worker_lookup(char *name, zactor_worker_map **map, size_t mapper_size) {
  size_t i = 0;
  for (i = 0; i < mapper_size; i++) {
    if (strcmp((*map[i]).name, name) == 0) {
	    return map[i]->worker;
    }
  }
  return NULL;
}

static void log_message(const char *message) {
  time_t ltime;
  struct tm *result;
  char *stime;

  ltime = time(NULL);
  result = localtime(&ltime);
  stime = asctime(result);
  strtok(stime, "\n");
  printf("[%s] %s\n", stime, message);
}

int main(int argc, char **argv) {
  // No longer requiring initial worker
  /*if (argc < 2) {
    printf("Usage: %s <disk-path>:<name> ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }*/

  static const struct option long_options[] = {
    {"help", no_argument, NULL, 'h'},
    {"verbose", no_argument, NULL, 'v'},
    {0, 0, 0, 0}
  };

  const char* usage =
    "Usage mimir [options]\n\n"
    "  -h --help\t\t\tShows this message\n"
    "  -v --verbose\t\t\tEnabled verbose mode\n"
    "\n";

  print_mimir_intro();

  int c = 0;
  while (1) {
    int option_index = 0;
    c = getopt_long(argc, argv, "hv", long_options, &option_index);
    if (c == -1) {
      break;
    }
    switch(c) {
      case 'h':;
        printf("%s", usage);
        exit (EXIT_SUCCESS);
      case 'v':
        log_message("Running in verbose mode");
        log_message("Exiting...");
        exit (EXIT_SUCCESS);
        break;
    }
  }

  // Zactor setup
  size_t worker_map_size = 0;
  zactor_worker_map **worker_map = NULL;

  // Initial worker setup
  for (int i = 0; i < argc - 1; i++) {
    worker_map_size++;
    worker_map = realloc(worker_map, sizeof(zactor_worker_map *) * worker_map_size);
    char *path = strtok(argv[i+1], ":");
    char *name = strtok(NULL, ":");
    mapping_zactor_worker(name, zactor_new((void *)worker_task, path), worker_map, worker_map_size);
  }

  management_worker_ref management_ref;
  management_ref.wmap_size = &worker_map_size;
  management_ref.wmap = &worker_map;

  // Management Socket Setup
  // worker_map is now owned by the management worker
  // memory is handled from within that zactor
  log_message("Setting up management worker");
  zactor_t *management_sock = zactor_new((void *)management_worker_task, (void*)&management_ref);

  // Setup daemon ipc endpoint
  char *ep = guestfs_inspect_endpoint();
  // Setting up endpoint log message
  char *ep_log = (char *)malloc((strlen(ep) * sizeof(char)) + 32);

  // Sending endpoint log message
  sprintf(ep_log, "Endpoint: %s", ep); // NOLINT
  log_message(ep_log);
  free(ep_log);

  // Setup ZMQ routers
  zsock_t *frontend = zsock_new(ZMQ_ROUTER);
  zsock_bind(frontend, "%s", ep);
  free(ep);

  zsock_t *backend = zsock_new(ZMQ_ROUTER);
  zsock_bind(backend, "inproc://workers");

  // Connecting frontend to backend
  zpoller_t *poller = zpoller_new(frontend, backend, NULL);
  assert(poller);

  log_message("Server listening for requests");
  if (worker_map == NULL) {
    log_message("No workers have been initialized");
  }

  zframe_t *message_pop = NULL;

  while (true) {
    void *sock = zpoller_wait(poller, -1);

    if (!sock) {
      break;
    }

    if (sock == frontend) {
      zmsg_t *msg = zmsg_recv(frontend);
      // reply id
      zframe_t *identity = zmsg_pop(msg);
      // Null frame
      message_pop = zmsg_pop(msg);
      // Destroy immediately
      zframe_destroy(&message_pop);

      struct guestfs_inpsect_command *c = guestfs_inspect_zmsg_to_command(msg);
      if (c == NULL) {
        log_message("Error creating inspect command");
        zmsg_destroy(&msg);
        zframe_destroy(&identity);
        continue;
      }

      zactor_t *zactor_tmp = zactor_worker_lookup(c->name, (*management_ref.wmap), worker_map_size);
      if (zactor_tmp != NULL) {
        // send request to worker
        zmsg_prepend(msg, &identity);
        zactor_send(zactor_tmp, &msg);
      } else {
        // Failure to lookup worker
        log_message("Request to invalid worker");
        zmsg_t *resp = zmsg_new();
        zmsg_pushstr(resp, "WORKER NOT FOUND");
        zmsg_pushstr(resp, "");
        zmsg_prepend(resp, &identity);
        // Sending response
        zmsg_send(&resp, frontend);
        zmsg_destroy(&resp);
      }

      zactor_tmp = NULL;
      guestfs_inspect_command_destroy(&c);
      zmsg_destroy(&msg);
      zframe_destroy(&identity);
    } else if (sock == backend) {
      zmsg_t *msg = zmsg_recv(backend);
      message_pop = zmsg_pop(msg); // Removing backend id
      // Destroy immediately
      zframe_destroy(&message_pop);
      zmsg_send(&msg, frontend);
      zmsg_destroy(&msg);
    } else {
      log_message("Socket found from unknown zsock");
    }
  }

  log_message("Cleaning up workers");
  // Cleanup
  //for (size_t i = 0; i < worker_map_size; i++) {
  //  zactor_destroy(&worker_map[i]->worker);
  //  free(worker_map[i]->name);
  //  free(worker_map[i]);
  //}
  //free(worker_map);

  // Management cleanup
  zactor_destroy(&management_sock);

  zpoller_destroy(&poller);
  zsock_destroy(&frontend);
  zsock_destroy(&backend);
  exit(EXIT_SUCCESS);
}

