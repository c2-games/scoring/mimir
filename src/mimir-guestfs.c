#include <stdlib.h>
#include <assert.h>
#include <guestfs.h>

void cat_file(guestfs_h *g, const char *file_path, char **file_content, size_t *file_size) {
  assert((*file_content) == NULL);
  if (guestfs_is_file_opts(g, file_path, GUESTFS_IS_FILE_OPTS_FOLLOWSYMLINKS, 1, -1) > 0) {
    (*file_content) = guestfs_read_file(g, file_path, file_size);
    if ((*file_content) == NULL) {
      exit(EXIT_FAILURE);
    }
  }
  return;
}

