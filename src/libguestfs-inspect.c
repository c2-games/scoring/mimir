#include <czmq.h>

#include "libguestfs-inspect.h"

char *guestfs_inspect_endpoint() {
  const char *guestfs_inspect_endpoint = getenv("GUESTFS_INSPECT_ENDPOINT");
  const char *socket_file_name = "/guestfs-inspect.sock";
  char *res = NULL;
  if (guestfs_inspect_endpoint) {
    res = malloc(strlen(guestfs_inspect_endpoint) + 1);
    strcpy(res, guestfs_inspect_endpoint); // NOLINT
  } else {
    const char *xdg_runtime_dir = getenv("XDG_RUNTIME_DIR");
    if (xdg_runtime_dir) {
      res = malloc(strlen(xdg_runtime_dir) + strlen(socket_file_name) + strlen("ipc://") + 1);
      strcpy(res, "ipc://"); // NOLINT
      strcat(res, xdg_runtime_dir); // NOLINT
      strcat(res, socket_file_name); // NOLINT
    } else {
      const char *tmp_dir = "/tmp";
      res = malloc(strlen(tmp_dir) + strlen(socket_file_name) + strlen("ipc://") + 1);
      strcpy(res, "ipc://"); // NOLINT
      strcat(res, tmp_dir); // NOLINT
      strcat(res, socket_file_name); // NOLINT
    }
  }
  return res;
}

zmsg_t *guestfs_inspect_command_to_zmsg(const struct guestfs_inpsect_command *command) {
  zmsg_t *res = zmsg_new();

  zmsg_addstr(res, command->name);
  zmsg_addmem(res, &(command->command), sizeof(command->command));

  switch (command->command) {
  case GUESTFS_COMMAND_LS:;
    zmsg_addmem(res, &(command->args.ls.paths_length), sizeof(command->args.ls.paths_length));

    for (size_t i = 0; i < (command->args.ls.paths_length); i++) {
      zmsg_addstr(res, command->args.ls.paths[i]);
    }
    break;
  case GUESTFS_COMMAND_CAT:;
    zmsg_addmem(res, &(command->args.cat.paths_length), sizeof(command->args.cat.paths_length));

    for (size_t i = 0; i < (command->args.cat.paths_length); i++) {
      zmsg_addstr(res, command->args.cat.paths[i]);
    }
    break;
  case GUESTFS_COMMAND_STATUS:;
    zmsg_addmem(res, &(command->args.status.worker_len), sizeof(command->args.status.worker_len));

    for (size_t i = 0; i < (command->args.status.worker_len); i++) {
      zmsg_addstr(res, command->args.status.workers[i]);
    }
    break;
  }

  return res;
}

struct guestfs_inpsect_command *guestfs_inspect_zmsg_to_command(zmsg_t *external_msg) {
  // This way we do not modify the argument we are given.
  zmsg_t *msg = zmsg_dup(external_msg);
  struct guestfs_inpsect_command *res = malloc(sizeof(struct guestfs_inpsect_command));

  if (!(zmsg_size(msg) > 2)) {
    free(res);
    zmsg_destroy(&msg);
    return NULL;
  }

  res->name = zmsg_popstr(msg);
  zframe_t *next = zmsg_pop(msg);
  res->command = *(zframe_data(next));
  zframe_destroy(&next);

  switch (res->command) {
  case GUESTFS_COMMAND_LS:;
    next = zmsg_pop(msg);
    res->args.ls.paths_length = *(zframe_data(next));
    zframe_destroy(&next);

    res->args.ls.paths = calloc(res->args.ls.paths_length, sizeof(char *));

    for (size_t i = 0; i < res->args.ls.paths_length; i++) {
      res->args.ls.paths[i] = zmsg_popstr(msg);
    }
    break;
  case GUESTFS_COMMAND_CAT:;
    next = zmsg_pop(msg);
    res->args.cat.paths_length = *(zframe_data(next));
    zframe_destroy(&next);

    res->args.cat.paths = calloc(res->args.cat.paths_length, sizeof(char *));

    for (size_t i = 0; i < res->args.cat.paths_length; i++) {
      res->args.cat.paths[i] = zmsg_popstr(msg);
    }
    break;
  case GUESTFS_COMMAND_STATUS:;
    next = zmsg_pop(msg);
    res->args.status.worker_len = *(zframe_data(next));
    zframe_destroy(&next);

    res->args.status.workers = calloc(res->args.status.worker_len, sizeof(char *));

    for (size_t i = 0; i < res->args.status.worker_len; i++) {
      res->args.status.workers[i] = zmsg_popstr(msg);
    }
    break;
  default:
    printf("Command not found\n");
  }
  zmsg_destroy(&msg);

  return res;
}

void guestfs_inspect_command_destroy(struct guestfs_inpsect_command **c) {
  free((*c)->name);
  switch ((*c)->command) {
  case GUESTFS_COMMAND_LS:;
    for (size_t i = 0; i < (*c)->args.ls.paths_length; i++) {
      free((*c)->args.ls.paths[i]);
    }
    free((*c)->args.ls.paths);
    break;
  case GUESTFS_COMMAND_CAT:;
    for (size_t i = 0; i < (*c)->args.cat.paths_length; i++) {
      free((*c)->args.cat.paths[i]);
    }
    free((*c)->args.cat.paths);
    break;
  case GUESTFS_COMMAND_STATUS:;
    for (size_t i = 0; i < (*c)->args.status.worker_len; i++) {
      free((*c)->args.status.workers[i]);
    }
    free((*c)->args.status.workers);
    break;
  }

  free(*c);
  *c = NULL;
}
