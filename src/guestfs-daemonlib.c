#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <guestfs.h>

#define STREQ(a, b) (strcmp((a), (b)) == 0)

static int compare_key_len(const void*, const void*);
static int count_mountpoints(char *const *argv);

static int count_mountpoints(char *const *argv) {
  size_t c;
  for (c = 0; argv[c]; c++) {}
  return c;
}

static int compare_key_len(const void *p1, const void *p2) {
  const char *key1 = *(char* const*) p1;
  const char *key2 = *(char* const*) p2;
  return strlen(key1) - strlen(key2);
}

char* mimir_guestfs_os_type(guestfs_h* g) {
  char *root = NULL;
  char *status = NULL;
  char **roots = NULL;
  char **mountpoints = NULL;

  if (g == NULL) {
    return status;
  }

  roots = guestfs_inspect_os(g);
  if (roots == NULL) {
    return status;
  }

  for (int i = 0; roots[i] != NULL; i++) {
    root = roots[i];
    mountpoints = guestfs_inspect_get_mountpoints(g, root);
    if (mountpoints == NULL) { // This probably should never happen?
      free(roots);
      free(mountpoints);
      return NULL;
    }

    // Sorting mountpoints to be in {'${device_path}', '${mount_path}'} format
    qsort(mountpoints, count_mountpoints(mountpoints) / 2, 2 * sizeof (char*), compare_key_len);
    for (int j = 0; mountpoints[j] != NULL; j += 2) {
      if (STREQ("/", mountpoints[j])) {
        status = guestfs_inspect_get_type(g, root);
      }
      free(mountpoints[j]);
      free(mountpoints[j+1]);
    }
    free(mountpoints);
    free(root);
  }

  free(roots);
  return status;
}

guestfs_h* init_ro_guestfs(const char *disk_path) {
  guestfs_h *g = NULL;
  char **roots, **mountpoints;
  char *root;
  size_t i, j;

  // Create a connection handle
  g = guestfs_create();
  if (g == NULL) {
    exit(EXIT_FAILURE);
  }

  // Adding disk_path to connection handle
  if (guestfs_add_drive_opts(g, disk_path, GUESTFS_ADD_DRIVE_OPTS_READONLY, 1, -1) == -1) {
    exit(EXIT_FAILURE);
  }

  // Launching connection handle
  if (guestfs_launch(g) == -1) {
    exit(EXIT_FAILURE);
  }

  // Pulling rootfs information
  roots = guestfs_inspect_os(g);
  if (roots == NULL) {
    exit(EXIT_FAILURE);
  }

  // Looping through roots to mount mountpoints
  for (j = 0; roots[j] != NULL; j++) {
    root = roots[j];
    mountpoints = guestfs_inspect_get_mountpoints(g, root);
    if (mountpoints == NULL) {
      exit(EXIT_FAILURE);
    }
    // Sorting mountpoints to be in {'${device_path}', '${mount_path}'} format
    qsort(mountpoints, count_mountpoints(mountpoints) / 2, 2 * sizeof (char*), compare_key_len);
    for (i = 0; mountpoints[i] != NULL; i += 2) {
      guestfs_mount_ro(g, mountpoints[i+1], mountpoints[i]);
      free(mountpoints[i]);
      free(mountpoints[i+1]);
    }
    free(mountpoints);
    free(root);
  }
  free(roots);
  return g;
}
