import sys
import zmq

#worker_name = sys.argv[1]
#worker_tag = sys.argv[2]

print(f"Attempting to list all workers")

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('ipc:///tmp/guestfs-mang-inspect.sock')

message = "32"
socket.send(message.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(''.encode('utf-8'))

#message = socket.recv()
#print(message.decode('utf-8'))

socket.close()
context.destroy()
