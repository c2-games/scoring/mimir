#include <stdio.h>
#include <stdlib.h>
#include <czmq.h>
#include <guestfs.h>

#include "libguestfs-inspect.h"

#define STREQ(a, b) (strcmp((a), (b)) == 0)

void print_help(char *);
enum guestfs_inspect_command_const parse_command(const char *);

int main(int argc, char **argv) {
  if (argc == 1) {
    print_help(argv[0]);
    return EXIT_SUCCESS;
  }

  char *ep = guestfs_inspect_endpoint();
  zsock_t *daemon = zsock_new_req(ep);
  free(ep);

  struct guestfs_inpsect_command *command = malloc(sizeof(struct guestfs_inpsect_command));
  command->name = calloc(strlen(argv[1]) + 1, sizeof(char));
  strcpy(command->name, argv[1]);
  command->command = parse_command(argv[2]);

  switch (command->command) {
  case GUESTFS_COMMAND_LS:
    command->args.ls.paths_length = 1;
    command->args.ls.paths = calloc(1, sizeof(char *));
    command->args.ls.paths[0] = calloc(strlen(argv[3] + 1), sizeof(char));
    strcpy(command->args.ls.paths[0], argv[3]);
    break;
  case GUESTFS_COMMAND_CAT:
    command->args.cat.paths_length = 1;
    command->args.cat.paths = calloc(1, sizeof(char *));
    command->args.cat.paths[0] = calloc(strlen(argv[3] + 1), sizeof(char));
    strcpy(command->args.cat.paths[0], argv[3]);
    break;
  }

  zmsg_t *msg = guestfs_inspect_command_to_zmsg(command);
  zmsg_send(&msg, daemon);
  zmsg_t *rep = zmsg_recv(daemon);

  char *res = NULL;
  // process reply
  switch (command->command) {
  case GUESTFS_COMMAND_LS:
    res = zmsg_popstr(rep);
    printf("Res:\n%s\n", res);
    free(res);
    break;
  case GUESTFS_COMMAND_CAT:
    res = zmsg_popstr(rep);
    printf("Res:\n%s\n", res);
    free(res);
    break;
  }

  zmsg_destroy(&msg);
  zmsg_destroy(&rep);
  zsock_destroy(&daemon);
  guestfs_inspect_command_destroy(&command);

  return EXIT_SUCCESS;
}

void print_help(char *name) {
  printf("Usage: %s [name] [command] <arguments>\n", name);
  printf("Commands:\n");
  printf("  ls  <path>\n");
  printf("  cat <path>\n");
}

enum guestfs_inspect_command_const parse_command(const char *input) {
  if (STREQ(input, "ls")) {
    return GUESTFS_COMMAND_LS;
  } else if (STREQ(input, "cat")) {
    return GUESTFS_COMMAND_CAT;
  }

  exit(EXIT_FAILURE);
}
