import sys
import zmq

worker_name = sys.argv[1]
disk_path = sys.argv[2]

print(f"Attempting to register worker {worker_name} at path {disk_path}")

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('ipc:///tmp/guestfs-mang-inspect.sock')

tt = 1
tt_size = 1

message = "01"
socket.send(message.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(worker_name.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(disk_path.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(''.encode('utf-8'))

message = socket.recv()
print(message.decode('utf-8'))

socket.close()
context.destroy()
