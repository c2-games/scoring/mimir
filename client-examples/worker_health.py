import sys
import zmq

worker_name = sys.argv[1]
#worker_tag = sys.argv[2]

print(f"Attempting to check worker {worker_name} health")

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('ipc:///tmp/guestfs-mang-inspect.sock')

message = "33"
socket.send(message.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(worker_name.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(''.encode('utf-8'))

message = socket.recv()
print(message.decode('utf-8'))

socket.close()
context.destroy()
