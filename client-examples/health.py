import zmq

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('ipc:///run/user/1000/guestfs-inspect.sock')

tt = 2
tt_size = 1

message = "rky8"
socket.send(message.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(tt.to_bytes(4, 'little'), flags=zmq.SNDMORE)
socket.send(tt_size.to_bytes(8, 'little'), flags=zmq.SNDMORE)
socket.send('/etc/os-release'.encode('utf-8'), flags=zmq.SNDMORE)
socket.send(''.encode('utf-8'))

message = socket.recv()
print(message.decode('utf-8'))

socket.close()
context.destroy()
