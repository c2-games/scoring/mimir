#ifndef LIBGUESTFS_INSPECT_H
#define LIBGUESTFS_INSPECT_H

#include <czmq.h>

enum guestfs_inspect_command_const {
  GUESTFS_COMMAND_LS,
  GUESTFS_COMMAND_CAT,
  GUESTFS_COMMAND_STATUS
};

struct guestfs_ls_args {
  size_t paths_length;
  char **paths;
};
struct guestfs_cat_args {
  size_t paths_length;
  char **paths;  
};
struct guestfs_status_args {
  size_t worker_len;
  char **workers;
};

struct guestfs_inpsect_command {
  char *name;
  enum guestfs_inspect_command_const command;
  union {
    struct guestfs_ls_args ls;
    struct guestfs_cat_args cat;
    struct guestfs_status_args status;
  } args;
};

char *guestfs_inspect_endpoint(void);
zmsg_t *guestfs_inspect_command_to_zmsg(const struct guestfs_inpsect_command *command);
struct guestfs_inpsect_command *guestfs_inspect_zmsg_to_command(zmsg_t *msg);
void guestfs_inspect_command_destroy(struct guestfs_inpsect_command **c);

#endif // LIBGUESTFS_INSPECT_H
